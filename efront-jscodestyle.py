#-*- coding: UTF-8 -*-

import urllib
import json
import sys
import re

user = sys.argv[1]
#user= 'hldiao'
print 'efront user:' + str(user)


def querySonarIssues(user,level):
    params = urllib.urlencode({'componentKeys': 'jscodestyle-' + user, 'severities': level})
    sonarInfo = urllib.urlopen("http://sonar.cqrd.x/api/issues/search?%s&&resolved=no" % params)
    sread = sonarInfo.read()
    issues = json.loads(sread)['total']
    print 'level ' + level + ' issues :' + str(issues)
    return issues

def querySonarMetric(user,key):
    params = urllib.urlencode({'componentKey': 'jscodestyle-' + user, 'metricKeys': key})
    sonarInfo = urllib.urlopen("http://sonar.cqrd.x/api/measures/component?%s" % params)
    sread = sonarInfo.read()
    issues = json.loads(sread)['component']['measures'][0]['value']
    print 'key ' + key + ' value :' + str(issues)
    return issues

Blocker = querySonarIssues(user,'BLOCKER')
Critical = querySonarIssues(user,'CRITICAL')
Major = querySonarIssues(user,'MAJOR')
if int(Blocker) != 0:
    print 'sonar阻断issue数(Blocker)不为0 ：' +Blocker + '%'
    sys.exit(-1)
if int(Critical) != 0:
    print 'sonar严重issue数(Critical)不为0：' +Critical + '%'
    sys.exit(-1)
if int(Major) != 0:
    print 'sonar主要issue数(Major)不为0：' +Major + '%'
    sys.exit(-1)

sqale_rating = querySonarMetric(user,'sqale_rating')
if float(sqale_rating) != 1.0:
    print '质量等级(sqale_rating)不为A ：' +sqale_rating
    sys.exit(-1)

efrontRUL = 'http://efront.cqrd.x/'
tokenXml = urllib.urlopen(efrontRUL + 'api2.php?action=token')
strTokenXml = str(tokenXml.read())
print 'efront strTokenXml :' + strTokenXml
token = re.search('<token>(.*)</token>', strTokenXml).group(1)
print 'efront token :' + token
loginParams = urllib.urlencode(
        {'token': token, 'action': 'login', 'username': 'apiuserpro', 'password': 'api2017gds'})
loginXml = urllib.urlopen(efrontRUL + 'api2.php?%s' % loginParams)
strLoginXml = str(loginXml.read())
print 'efront strLoginXml :' + strLoginXml
efrontParams = urllib.urlencode(
        {'action': 'completeLession', 'userEmail': user, 'lessionId': '216', 'token': token})
efrontInfo = urllib.urlopen(efrontRUL + 'api_jobskill.php?%s' % efrontParams)
strEfrontResultXml = str(efrontInfo.read())
print 'efront result :' + strEfrontResultXml
efrontresult = re.search('<result>(.*)</result>', strEfrontResultXml).group(1)
if efrontresult != 'true':
    sys.exit(-1)
else:
    print 'efront pass'
