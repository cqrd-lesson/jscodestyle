/**
 * 将文件中不规范的代码修改规范
 */


var superman = {
  clazz: "alien",
  name:"superman",
  age:"30"
};

/**
 * 初始化数组
 */
var intialArr = function(){
	var someStack = [];
	someStack[someStack.length] = "abracadabra";
};

//获取全名
var getFullName = function(firstName,lastName){
	var name = null;
	if (!firstName){
		name = "hong";
	}
	var fullName = name + lastName;
	return fullName;
};

/**
 * @param value 传递的参数值
 */
var getKeyValue = function(value){
	var key = "";
	switch (value){
	    case "Normal":
			key = 0;
			break;
	    case "TC":
			key = 1;
			break;
	    case "Admin":
			key = 2;
			break;
		default:
			key = "";
	}
	return key;
};

/**
 * 将数组转换成json对象
 * @param attr 数组
 */
var arrayToJson = function (attr){
    var obj = {};
    for(var i=0; i<attr.length; i++){
        for(var key in attr[i] ){
            if("usertype" == attr[i].key){
                obj[key] = getKeyValue(attr[i].value);
            }else if("username" == attr[i][key]){
                obj[key] = attr[i].value.replace(/\s/,"");
            }else{
				obj[key] = attr[i].value;
			}
        }
    }
	// obj.address = eval("obj.address1") + "," + eval("0obj.address2");
	obj.address = obj.address1 +"," + obj.address2;
    return obj;
};


/**
 * 统计分数大于9的用户数量
 * @param userList 用户列表
 */
var getCountAmount = function(userList){
	var userListLen = userList.length;
	var count = 0;
	
	for (var i =0;i<userListLen;i++){
		if (userList[i].score > 9){
			count++;
		}
	}
	return  count;
};
