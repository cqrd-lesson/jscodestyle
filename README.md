本教程源码托管在 <https://gitlab.cae.com/course/technology/tree/master>，如果发现本门课中的任何错误或有好的意见请在该托管页面的`issue` 中提交。

### 作业要求和说明
1.请参考 [gitlab使用手册](https://gitlab.cae.com/course/doc/blob/master/manual.gitlab.md) 在 gitlab 上建立一个名为 efront的项目，已经有的略过

2.按照测验要求完成测验或练习

3.提交题目至个人项目的master

4.阅卷说明：

根据题目要求进行功能开发，测验结果在每次代码提交后会进行自动检测，直到自动检测通过，该课程即通过